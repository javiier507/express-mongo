var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    User = mongoose.model('Users');

module.exports = function (app) 
{
    app.use('/', router);
};

router.get('/', function (request, response) 
{
	User.find(function(err, documen)
	{
		if(err)
		{
			response.status(500);
		}
		else
		{
			response.render('index', {
				title: 'Lista Usuarios', 
				users: documen
			});
		}
	});
});

router.get('/crear', function(request, response)
{
    response.render('create', {
    	type: 'Crear'
    });
});

router.post('/crear', function(request, response)
{
	var user = new User({
		name: 		request.body.nombre,
		lastname: 	request.body.apellido,
		username: 	request.body.usuario,
		email: 		request.body.correo,
		password: 	request.body.contrasena,
		image: 		request.body.imagen
	});

	user.save(function(err, documen, count)
	{
		if(err)
		{
			response.status(500).send('Error En Guardar Usuario');
		}
		else
		{
			response.render('create', {
				type: 'Crear', 
				exito: count
			});
		}
	});
});

router.get('/user/:username', function(request, response)
{
	var condicion = {username: request.params.username};
	User.findOne(condicion, function(err, documen)
	{
		if(!documen)
		{
			response.redirect('/');
		}
		else
		{
			response.render('update', {
				type: 'Actualizar',
				user: documen
			});
		}	
	});
});

router.put('/editar/:username', function(request, response)
{
	var condicion = {username: request.params.username};
	var user = {
		name: 		request.body.nombre,
		lastname: 	request.body.apellido,
		username: 	request.body.usuario,
		email: 		request.body.correo
	};
	
	User.findOneAndUpdate(condicion, user).exec(function(err, documen)
	{
		if(documen)
		{
			response.status(200).send(documen);	
		}
		else
		{
			response.status(404).send('User 404');
		}
	});
});

router.delete('/eliminar/:username', function(request, response)
{
	var username = request.params.username;
	User.findOneAndRemove({username: username}, function(err, documen)
	{
		if(documen)
		{
			response.status(200).send(documen);
		}
		else
		{
			response.status(400).send('User 404');
		}	
	});
});