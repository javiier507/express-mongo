$(document).ready(inicio);

function inicio()
{
    $('#eliminar').click(eliminar);
    $('#formEditar').submit(editar);
}

function eliminar(event)
{
    event.preventDefault();
    var username = $(this).data('username');

    $.ajax({
        async:true,
        type: "DELETE",
        dataType: "HTML",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url:"http://localhost:3000/eliminar/"+username,
        timeout:4000,
        success: exito
    });

    refrescar();
}

function editar(event)
{
    event.preventDefault();
    var user = $(this).serialize();
    var username = $('#usuario').val();
    console.log(user);

    $.ajax({
        async:true,
        type: "PUT",
        dataType: "HTML",
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        url:"http://localhost:3000/editar/"+username,
        data: user,
        timeout:4000,
        success: exito
    });

    refrescar();
}

function exito(respuesta)
{
    console.log(respuesta);

    if(respuesta)
    {
        var mensaje = '';
        mensaje += '<div data-alert class="alert-box success radius">';
        mensaje += '<p>Operacion Exitosa</p>';
        mensaje += '<a href="#" class="close">&times;</a>';
        mensaje += '</div>';
        $('#mensajes').html(mensaje);
    }   
}

function refrescar()
{
    setTimeout(function()
    {
        window.location.href = '/';    
    }, 3000);
}