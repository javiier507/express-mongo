var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'express-mongo'
    },
    port: 3000,
    db: 'mongodb://localhost/users'
  },

  test: {
    root: rootPath,
    app: {
      name: 'express-mongo'
    },
    port: 3000,
    db: 'mongodb://localhost/users'
  },

  production: {
    root: rootPath,
    app: {
      name: 'express-mongo'
    },
    port: 3000,
    db: 'mongodb://localhost/express-mongo-production'
  }
};

module.exports = config[env];
